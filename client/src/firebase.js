// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: import.meta.env.VITE_FIREBASE_API_KEY,
  authDomain: "estate-mern-da8ea.firebaseapp.com",
  projectId: "estate-mern-da8ea",
  storageBucket: "estate-mern-da8ea.appspot.com",
  messagingSenderId: "660988923927",
  appId: "1:660988923927:web:b4504ed041e6301c2b8c52"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);